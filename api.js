const swapi = require('swapi-node');

function mongoConnect(db, cb) {
    let MongoClient = require('mongodb').MongoClient;
    let uri = "mongodb+srv://app:8siiPszDKS8PUtH@cluster0-qmhuw.mongodb.net/test?retryWrites=true";
    MongoClient.connect(uri, function(err, client) {
        if (err) cb(err);
        let dbo = client.db(db);
        cb(dbo);
        client.close();
    });
}

function mongoCollection(db, collection, cb) {
	mongoConnect(db, function(dbo) {
		if(dbo) {
            dbo.collection(collection).find().toArray(function(err, result) {
                cb(result);
            });
		}
    });
}

function mongoCollectionQueryString(key_string, value, db, collection, cb) {
    mongoConnect(db, function(dbo) {
        if(dbo) {
            let query_string = null;
        	let ObjectId = require('mongodb').ObjectId;

            if(ObjectId.isValid(value)) {
                query_string = { "_id": ObjectId(value) };
			} else {
                query_string = { [key_string]: value };
			}

            dbo.collection(collection).find(query_string).toArray(function(err, result) {
                if (err) cb(err);
                cb(result);
            });
        }
    });
}

function mongoCollectionInsert(query_string, db, collection, cb) {
    mongoConnect(db, function(dbo) {
        if(dbo) {
			dbo.collection(collection).insertOne(query_string, function(err, result) {
                if (err) cb(err);
                cb(result);
            });
        }
    });
}

function mongoCollectionRemove(value, db, collection, cb) {
    mongoConnect(db, function(dbo) {
        if(dbo) {
            let ObjectId = require('mongodb').ObjectId;
            
            let query_string = { "_id": ObjectId(value) };
            dbo.collection(collection).deleteOne(query_string, function(err, result) {
                if (err) cb(err);
                cb(result);
            });
        }
    });
}

function getInfoPlanet(info, cb) {
	swapi.get('http://swapi.co/api/planets').then((result) => {
		switch(info) {
			case 'list':
				let list = JSON.parse(JSON.stringify(result));
				cb(list);
				break;
			case 'count':
				let count = JSON.parse(JSON.stringify(result)).count;
				cb(count);
				break;
		}
	});
}

function getRandomPlanet(cb) {
	getInfoPlanet('count', function(planet_id) {
		if(planet_id) {
			let random_int = Math.floor(Math.random() * planet_id) + 1;
			swapi.get('http://swapi.co/api/planets/' + random_int).then((result) => {
				let planet_data = JSON.parse(JSON.stringify(result));
				cb(planet_data);
			});
		}
	});	
}

exports.mongoConnect				= mongoConnect;
exports.mongoCollection				= mongoCollection;
exports.mongoCollectionQueryString	= mongoCollectionQueryString;
exports.mongoCollectionInsert		= mongoCollectionInsert;
exports.mongoCollectionRemove       = mongoCollectionRemove;
exports.getInfoPlanet				= getInfoPlanet;
exports.getRandomPlanet				= getRandomPlanet;