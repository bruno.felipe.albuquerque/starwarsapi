const express		= require('express');
const app			= express();
const api			= require('./api.js');
const path			= require('path');
const bodyParser	= require("body-parser");

app.set('view engine', 'ejs');
app.set('views', path.join(__dirname, 'views'));
app.use(express.static('public'));
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());

app.get('/', function (req, res) {
    api.getCollectionToArray('planets', function(planet_list) {
        if(planet_list === Object(planet_list)) {
            res.render('list', {planets: planet_list, error: null});
        } else {
            res.render('list', {planets: null, error: 'No Planets Found!'}); //VAZIO
        }
    });
});

app.post('/search', function (req, res) {
    var search_string = req.body.search_string;

    console.log(search_string);

    api.getCollectionToArray('planets', function(planet_list) {
        if(planet_list === Object(planet_list)) {
            res.render('list', {planets: planet_list, error: null});
        } else {
            res.render('list', {planets: null, error: 'No Planets Found!'}); //VAZIO
        }
    });
});