const express		= require('express');
const app			= express();
const api			= require('./api.js');
const path			= require('path');
const alert         = require('alert-node');
const bodyParser	= require("body-parser");

const { body,validationResult } = require('express-validator/check');
const { sanitizeBody }          = require('express-validator/filter');

app.set('view engine', 'ejs');
app.set('views', path.join(__dirname, 'views'));
app.use(express.static('public'));
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());

/* GET */

app.get('/', function (req, res) {
	res.render('index', {});
});

app.get('/add', function (req, res) {
    res.render('add', {});
});

app.get('/random', function (req, res) {
    api.getRandomPlanet(function(planet_data) {
        if(planet_data === Object(planet_data)) {
            res.render('random', {name: planet_data.name, climate: planet_data.climate, terrain: planet_data.terrain, message: null, error: null});
        } else {
            res.render('random', {name: null, climate: null, terrain: null, message: null, error: 'Error, please try again.'});
        }
    });
});

app.get('/search', function (req, res) {
    res.render('list', {planets: null, error: 'No Planets Found!'});
});

app.get('/list', function (req, res) {
    var delete_key = req.query.delete;

    if(delete_key) {
        api.mongoCollectionRemove(delete_key, 'swapi', 'planets', function(result) {
            if (result) {
                alert('Planet Removed!');
            } else {
                alert('Error removing the Planet!');
            }

            api.mongoCollection('swapi', 'planets', function(planet_list) {
                if(planet_list === Object(planet_list)) {
                    res.render('list', {planets: planet_list, error: null});
                } else {
                    res.render('list', {planets: null, error: 'No Planets Found!'});
                }
            });
        });
    } else {
        api.mongoCollection('swapi', 'planets', function(planet_list) {
            if(planet_list === Object(planet_list)) {
                res.render('list', {planets: planet_list, error: null});
            } else {
                res.render('list', {planets: null, error: 'No Planets Found!'});
            }
        });
    }
});

/* POST */
app.post('/random', function (req, res) {
	api.getRandomPlanet(function(planet_data) {
		if(planet_data === Object(planet_data)) {
			res.render('random', {name: planet_data.name, climate: planet_data.climate, terrain: planet_data.terrain, message: null, error: null});
		} else {
			res.render('random', {name: null, climate: null, terrain: null, message: null, error: 'Error, please try again.'});
		}
	});
});

app.post('/list', function (req, res) {
    var delete_key = req.query.delete;

    if(delete_key) {
        api.mongoCollectionRemove(delete_key, 'swapi', 'planets', function(result) {
            if (result) {
                alert('Planet Removed!');
            } else {
                alert('Error removing the Planet!');
            }

            api.mongoCollection('swapi', 'planets', function(planet_list) {
                if(planet_list === Object(planet_list)) {
                    res.render('list', {planets: planet_list, error: null});
                } else {
                    res.render('list', {planets: null, error: 'No Planets Found!'});
                }
            });
        });
    } else {
        api.mongoCollection('swapi', 'planets', function(planet_list) {
            if(planet_list === Object(planet_list)) {
                res.render('list', {planets: planet_list, error: null});
            } else {
                res.render('list', {planets: null, error: 'No Planets Found!'});
            }
        });
    }
});

app.post('/search', function (req, res) {
    var search_string = req.body.search_string;
    api.mongoCollectionQueryString('name', search_string, 'swapi', 'planets', function(planet_list) {
        if(planet_list === Object(planet_list)) {
            res.render('list', {planets: planet_list, error: null});
        } else {
            res.render('list', {planets: null, error: 'No Planets Found!'});
        }
    });
});

app.post('/add', function (req, res) {
    var name         = req.body.planet_name;
    var climate      = req.body.planet_climate;
    var terrain      = req.body.planet_terrain;
    var query_string = {name: name, climate: climate, terrain: terrain};

    if(name || climate || terrain) {
        api.mongoCollectionInsert(query_string, 'swapi', 'planets', function (result) {
            if (result) {
                alert('New Planet Inserted!');
                res.render('add', {});
            } else {
                alert('Error inserting a new Planet!');
                res.render('add', {message: ''});
            }
        });
    } else {
        res.render('add', {});
    }
});

/* OTHER */

app.listen(3000, function () {
	console.log('Server running at http://localhost:3000!');
});